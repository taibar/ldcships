import sys
import numpy as np
from definitions import define

definition = [
    ('Change', np.object_, 15, ""),
    ('Country', np.object_, 15, ""),
    ('Location', np.object_, 20, ""),
    ('Name', np.object_, 500, ""),
    ('NameWoDiacritics', np.object_, 500, ""),
    ('Subdivision', np.object_, 20, ""),
    ('Function', np.object_, 20, ""),
    ('Status', np.object_, 20, ""),
    ('Date', np.int_, 10, 0000),
    ('IATA', np.object_, 20, ""),
    ('Coordinates', np.object_, 100, ""),
    ('Remarks', np.object_, 1000, "")
]

cols    = []
dtypes  = {}
lens    = {}
na_vals = {}


CHANGE_RAW                          = None
COUNTRY_RAW                         = None
LOCATION_RAW                        = None
NAME_RAW                            = None
NAMEWODIACRITICS_RAW                = None
SUBDIVISION_RAW                     = None
FUNCTION_RAW                        = None
STATUS_RAW                          = None
DATE_RAW                            = None
IATA_RAW                            = None
COORDINATES_RAW                     = None
REMARKS_RAW                         = None

_CODE                               = 'code'
define(sys.modules[__name__], "RAW", definition, cols, dtypes, lens, na_vals)

