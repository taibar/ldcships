
def define(module, ext, definition, cols, dtypes, lens, na_vals):
    for __name, __dtype, __len, __na_val in definition:
        tmp = __name.upper()
        key = tmp + "_" + ext

        setattr(module, key, __name)
        cols.append(__name)

        dtypes[__name] = __dtype
        lens[__name] = __len
        na_vals[__name] = __na_val
