Countries = [
    'IS',
    'GL',
    'CA',
    'US',
    'VE',
    'BR',
    'AR',
    'IE',
    'GB',
    'PT',
    'ES',
    'FR',
    'BE',
    'NL',
    'DE',
    'DK',
    'NO',
    'SE',
    'FI',
    'IT',
    'GI'
]

USA = [
    'TX',
    'LA',
    'MS',
    'AL',
    'FL',
    'GA',
    'SC',
    'NC',
    'VA',
    'MD',
    'NJ',
    'NY',
    'CT',
    'RI',
    'MA',
    'NH',
    'ME'
]

Canada = [
    'NB',
    'PE',
    'QC',
    'NS',
    'ON',
    'NL'
]
