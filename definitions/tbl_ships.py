import sys
import numpy as np
from definitions import define

definition = [
    ('imo', np.int_, 15, 0),
    ('date', np.object_, 15, ""),
    ('latitude', np.float_, 20, 0.0),
    ('longitude', np.float_, 20, 0.0),
    ('destination', np.object_, 500, ""),
    ('bearing', np.float, 20, -9999.99),
    ('size', np.object_, 10, ""),
    ('zone', np.bool_, 10, False),
    ('statusLDC', np.object_, 10, ""),
    ('draft', np.float_, 10, 0.0)
]

cols    = []
dtypes  = {}
lens    = {}
na_vals = {}


IMO_RAW                     = None
DATE_RAW                    = None
LATITUDE_RAW                = None
LONGITUDE_RAW               = None
DESTINATION_RAW             = None
BEARING_RAW                 = None
SIZE_RAW                    = None
ZONE_RAW                    = None
STATUSLDC_RAW               = None
DRAFT_RAW                   = None

_UNIQUE_ID                  = 'unique_id'
_FIRST_KNOWN_LATITUDE       = 'first_known_latitude_for_voyage'
_FIRST_KNOWN_LONGITUDE      = 'first_known_longitude_for_voyage'
_LAST_KNOWN_LATITUDE        = 'last_known_latitude_for_voyage'
_LAST_KNOWN_LONGITUDE       = 'last_known_longitude_for_voyage'
_HAVERSINE                  = 'haversine_distance_to_panama'
_DIRECTION                  = 'direction_of_travel_wrt_panama'
_SPEED                      = 'speed_of_vessel'
_IS_PACIFIC_PORT            = 'is_pacific_port'
_IS_GOING_THROUGH           = 'is_going_through_panama'
_MOST_LIKELY_PORT           = 'most_likely_destination'
_ON_TRAJECTORY              = 'on_trajectory'
_IS_LOCODE                  = 'is_locode'
_LO_CODE                    = 'locode'
_FORMATTED_DESTINATION      = 'formatted_destination'
_INDICATOR                  = 'indicator'

define(sys.modules[__name__], "RAW", definition, cols, dtypes, lens, na_vals)

