import settings
import os
from settings import output_info, output_warning
from helpers import processing_helpers, port_helpers, data_helpers, shipping_helpers, database_helpers, regression_helpers
import platform
import multiprocessing
import time
from definitions import tbl_ships


def clearlogs():
    for file in os.listdir(settings.LOGS_ROOT):
        try:
            if file.lower().endswith('.log'):
                with open(os.path.join(settings.LOGS_ROOT, file), mode='w'):
                    pass

        except Exception as e:
            output_warning('Failed to clear old logs. Exception raised: {}'.format(e))

    output_info('Cleared old logs...')


def saveresults(df_in, df_out):
    try:
        for file in os.listdir(settings.OUTPUT_ROOT):
            try:
                if file.lower().endswith('.csv'):
                    os.unlink(os.path.join(settings.OUTPUT_ROOT, file))
            except Exception as e:
                output_warning('Failed to clear old predicted ship data from output. Exception raised: {}'.format(e))

        df_in.to_csv(os.path.join(settings.OUTPUT_ROOT, 'predicted_ships.csv'))
        df_out.to_csv(os.path.join(settings.OUTPUT_ROOT, 'rejected_ships.csv'))
    except Exception as e:
        output_warning('Failed to save outputs. Exception raised: {}'.format(e))
    return 0


def main():

    # Clear logs
    if settings.CLEARLOGS:
        clearlogs()

    # Unpack data
    if settings.UNPACK:
        output_info('Unpacking data...')
        data_helpers.unpacker()
    else:
        output_info('Not scheduled to unpack data...')

    # Rebuild database if needed
    if settings.REBUILD_DB:
        database_helpers.db_manager()

    # Run logic
    if settings.RUN:

        # Get data and initial filter
        df = data_helpers.collector()

        if settings.DEMO:
            output_info('Running in demo mode, will only process smaller subset of vessels...')
            df = df.loc[df[tbl_ships.IMO_RAW].isin(list(df[tbl_ships.IMO_RAW].unique()[:200]))]

        df = shipping_helpers.vesselsizer(df)

        # Identify unique voyages for each vessel log
        if not settings.SKIPFUZZYMATCH:
            df = processing_helpers.spinupmanager(df)
        df_original = df

        # Get relevant ships
        df = shipping_helpers.vesseltracker(df)
        #df = regression_helpers.onehotraw(df)
        df_copy = df

        # Match to ports
        df = port_helpers.portmanager(df)

        # Pass cleaned data to XGBoost
        model = regression_helpers.builder(df)

        # Output
        if settings.PREDICT:
            df_rgr = df_copy.loc[~df_copy[tbl_ships._UNIQUE_ID].isin(df[tbl_ships._UNIQUE_ID].unique())]
            df = regression_helpers.predict(model, df_rgr)
            df_in = df_original.loc[df_original[tbl_ships._UNIQUE_ID].isin(df[tbl_ships._UNIQUE_ID].unique())]
            df_out = df_original.loc[~df_original[tbl_ships._UNIQUE_ID].isin(df[tbl_ships._UNIQUE_ID].unique())]
            saveresults(df_in[tbl_ships.cols], df_out[tbl_ships.cols])

    return 0


if __name__ == '__main__':
    output_info('Platform identifier for multicompute: {}'.format(platform.system()))
    if platform.system() != 'Windows':
        multiprocessing.set_start_method('spawn')

    start_time = time.perf_counter()
    main()
    end_time = time.perf_counter()
    output_info('Finished! Time taken: {} seconds'.format(end_time - start_time))
