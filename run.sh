#!/bin/bash

# Usage info
show_help() {
cat << EOF
Usage: ${0##*/} [-h] [-b] [-r]...
Run or build and run the ldcships package.

	-h          display this help and exit
	-b          build and run the package (use if this is a fresh copy)
	-r	    run the package only (note that you need to build the package first!)
EOF
}

build_run(){
pip3 install virtualenv
virtualenv -p python3 venv
source venv/bin/activate &
pip3 install -r requirements.txt
python3 main.py
}

run(){
source venv/bin/activate &
python3 main.py
}


case $1 in
	-h)
		show_help
		pid=$!
		;;

	-b)
		build_run
		pid=$!
		;;

	-r)
		run
		pid=$!
		;;

	-?*)
		printf 'WARNING: unknown option: %s\n' "$1" >&2
		;;
		
	*)
		show_help
		pid=$!
		exit
esac
shift


pid=$!
trap "kill ${pid}; exit 1" INT
wait
