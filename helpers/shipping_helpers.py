import pandas as pd
from definitions import tbl_ships
import settings
from settings import output_warning, output_info
from math import radians, cos, sin, atan2, sqrt
from difflib import SequenceMatcher

def vesselsizer(df):
    try:
        output_info('Finding only vessels that can fit through the canal...')
        df = df.loc[(df[tbl_ships.SIZE_RAW] != 'CAPE') & (df[tbl_ships.SIZE_RAW] != 'VLOC') &
                    (df[tbl_ships.DRAFT_RAW] <= settings.MAX_DRAFT) & (df[tbl_ships.ZONE_RAW] == False)]
    except Exception as e:
        output_warning('Failed to size vessels correctly. Exception raised: {}'.format(e))
    return df


def vesseltracker(df):
    try:
        l = df.groupby([tbl_ships._UNIQUE_ID]).ngroups
        output_info('Determining vessel trajectory for {} vessels...'.format(l))

        df = df.groupby([tbl_ships._UNIQUE_ID]).apply(positionfinder)

        # df = df.loc[((df[df_ships._DIRECTION] == 'Towards') & (df[df_ships._ON_TRAJECTORY] == 'y') &
        #              (df[df_ships._HAVERSINE] <= settings.NBR_DAYS*24*df[df_ships._SPEED])) |
        #             ((df[df_ships._DIRECTION] == 'Stationary') & (df[df_ships._HAVERSINE] <= settings.WAITING_AREA))]

    except Exception as e:
        output_warning('Failed to determine vessel trajectory properties. Exception raised: {}'.format(e))
    return df


def vessellocator(df):
    try:
        output_info('Determining vessel location...')
        df = df.loc[((df[tbl_ships._DIRECTION] == 'Towards') & (df[tbl_ships._ON_TRAJECTORY] == 'y') &
                     (df[tbl_ships._HAVERSINE] <= settings.NBR_DAYS*24*df[tbl_ships._SPEED])) |
                    ((df[tbl_ships._DIRECTION] == 'Stationary') & (df[tbl_ships._HAVERSINE] <= settings.WAITING_AREA))]

    except Exception as e:
        output_warning('Failed to determine vessel location properties. Exception raised: {}'.format(e))
    return df


def positionfinder(df):
    try:

        df = df.sort_values(by=tbl_ships.DATE_RAW, ascending=False)
        iter = 0

        for index, row in df.iterrows():
            if iter + 1 < len(df):
                if row[tbl_ships._MOST_LIKELY_PORT] != df[tbl_ships._MOST_LIKELY_PORT].iloc[iter + 1]:
                    df = df.iloc[:iter+1]
                    break
                else:
                    iter += 1

        time_traveled = (max(df[tbl_ships.DATE_RAW]) - min(df[tbl_ships.DATE_RAW])).total_seconds() / (60 * 60)
        first_lat = df[tbl_ships.LATITUDE_RAW].loc[df[tbl_ships.DATE_RAW] == min(df[tbl_ships.DATE_RAW])].iloc[0]
        first_lon = df[tbl_ships.LONGITUDE_RAW].loc[df[tbl_ships.DATE_RAW] == min(df[tbl_ships.DATE_RAW])].iloc[0]
        last_lat = df[tbl_ships.LATITUDE_RAW].loc[df[tbl_ships.DATE_RAW] == max(df[tbl_ships.DATE_RAW])].iloc[0]
        last_lon = df[tbl_ships.LONGITUDE_RAW].loc[df[tbl_ships.DATE_RAW] == max(df[tbl_ships.DATE_RAW])].iloc[0]

        df[tbl_ships._FIRST_KNOWN_LATITUDE] = first_lat
        df[tbl_ships._FIRST_KNOWN_LONGITUDE] = first_lon
        df[tbl_ships._LAST_KNOWN_LATITUDE] = last_lat
        df[tbl_ships._LAST_KNOWN_LONGITUDE] = last_lon

        first_lat_rad = radians(first_lat)
        panama_lat_rad = radians(settings.PANAMA_LAT)
        last_lat_rad = radians(last_lat)

        delta_lat_last_panama_rad = radians(last_lat-settings.PANAMA_LAT)
        delta_lon_last_panama_rad = radians(last_lon-settings.PANAMA_LONG)

        delta_lat_first_panama_rad = radians(first_lat - settings.PANAMA_LAT)
        delta_lon_first_panama_rad = radians(first_lon - settings.PANAMA_LONG)

        delta_lat_first_last_rad = radians(last_lat - first_lat)
        delta_lon_first_last_rad = radians(last_lon - first_lon)

        a_last = sin(delta_lat_last_panama_rad/2)*sin(delta_lat_last_panama_rad/2) + \
            cos(panama_lat_rad)*cos(last_lat_rad)*sin(delta_lon_last_panama_rad/2)*sin(delta_lon_last_panama_rad/2)

        c_last = 2*atan2(sqrt(a_last), sqrt(1-a_last))

        latestdistance = round(c_last * settings.RADIUS)

        df[tbl_ships._HAVERSINE] = latestdistance
        output_info('Haversine distance to Panama for vessel {} on voyage ID  {} is {} km'.
                    format(df[tbl_ships.IMO_RAW].iloc[0], df[tbl_ships._MOST_LIKELY_PORT].iloc[0], df[tbl_ships._HAVERSINE].iloc[0]))

        a_first = sin(delta_lat_first_panama_rad/2)*sin(delta_lat_first_panama_rad/2) + \
            cos(panama_lat_rad)*cos(first_lat_rad)*sin(delta_lon_first_panama_rad/2)*sin(delta_lon_first_panama_rad/2)

        c_first = 2*atan2(sqrt(a_first), sqrt(1-a_first))

        initialdistance = round(c_first * settings.RADIUS)

        a_total = sin(delta_lat_first_last_rad / 2) * sin(delta_lat_first_last_rad / 2) + \
                  cos(last_lat_rad) * cos(first_lat_rad) * sin(delta_lon_first_last_rad / 2) * sin(
            delta_lon_first_last_rad / 2)

        c_total = 2 * atan2(sqrt(a_total), sqrt(1 - a_total))

        travelleddistance = round(c_total * settings.RADIUS)

        traveldirection = initialdistance - latestdistance

        ratio = abs(((travelleddistance + latestdistance) / initialdistance) -1)

        if traveldirection > 0:
            df[tbl_ships._DIRECTION] = 'Towards'
            df[tbl_ships._SPEED]     = round(abs(travelleddistance) / time_traveled)
            if ratio <= settings.OFFCOURSE_THRESH:
                df[tbl_ships._ON_TRAJECTORY] = 'y'
            else:
                df[tbl_ships._ON_TRAJECTORY] = 'n'

        elif traveldirection < 0:
            df[tbl_ships._DIRECTION] = 'Away'
            df[tbl_ships._SPEED]     = round(abs(travelleddistance) / time_traveled)
            df[tbl_ships._ON_TRAJECTORY] = 'n'
        else:
            df[tbl_ships._DIRECTION] = 'Stationary'
            df[tbl_ships._SPEED]     = 0
            df[tbl_ships._ON_TRAJECTORY] = 'm'


        # Keep last position
        df = df.loc[df[tbl_ships.DATE_RAW] == min(df[tbl_ships.DATE_RAW])].iloc[0]

    except Exception as e:
        output_warning('Unable to calculate Haversine distances for vessel {} on voyage ID {}. Exception raised: {}'.
                       format(df[tbl_ships.IMO_RAW].iloc[0], df[tbl_ships._MOST_LIKELY_PORT].iloc[0], e))
    return df


def getships(df):
    try:
        df = df.loc[df[tbl_ships._IS_GOING_THROUGH] == 'Going through']
    except Exception as e:
        output_warning('Failed to get list of predicted ships. Exception raised: {}'.format(e))
    return df