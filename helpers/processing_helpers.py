from definitions import tbl_ships
from settings import output_info, output_warning
from helpers.data_helpers import formatter
from multiprocessing import Process, Lock, Queue
from math import ceil
import pandas as pd
import settings
import multiprocessing
import platform
from helpers import data_helpers


def spinupmanager(df):

    df_y = df.loc[df[tbl_ships._IS_LOCODE] == 'y']
    s = pd.Series(data=df_y[tbl_ships._FORMATTED_DESTINATION])
    df_y = df_y.assign(most_likely_destination=s)


    df_n = df.loc[df[tbl_ships._IS_LOCODE] == 'n']
    try:
        output_info('About to fuzzy match and clean destination logs for {} vessels...'.
                    format(len(df_n[tbl_ships.IMO_RAW].unique())))

        if settings.MULTIPROC:
            if settings.HIGH_POWER:
                df_n = spinup(df_n)
                df = df_n.append(df_y)
                df[tbl_ships._UNIQUE_ID] = (
                        df[tbl_ships.IMO_RAW].astype(str) + df[tbl_ships._FORMATTED_DESTINATION] +
                        df[tbl_ships.STATUSLDC_RAW] + df[tbl_ships.DRAFT_RAW].astype(str)).apply(hash)
            else:
                s = df_n[tbl_ships.IMO_RAW].value_counts()
                idx_lst = []
                sum = 0
                totalsum = 0
                iter = 1
                r_df = pd.DataFrame()
                split = int(ceil(len(df_n)/settings.NBR_LINE))

                output_info('Low performance flag set; there are {} entries to process and data will be split into {}'
                            ' sets for processing...'.format(len(df_n), split))

                if s.sum() >= settings.NBR_LINE:
                    for index, item in s.iteritems():
                        sum += item
                        totalsum += item
                        if (sum <= settings.NBR_LINE) & (totalsum < s.sum()):
                            idx_lst.append(index)
                        else:

                            tmp = df_n.loc[df_n[tbl_ships.IMO_RAW].isin(idx_lst) == True]
                            output_info('Spinning up processes for set {} containing {} entries for {} vessels...'.format(
                                iter, len(tmp), len(idx_lst)))
                            r_df = r_df.append(spinup(tmp))
                            iter += 1
                            sum = item
                            idx_lst = []
                            idx_lst.append(index)
                else:
                    tmp = df_n
                    output_info('Spinning up processes for set {} containing {} entries for {} vessels...'.format(
                        iter, len(tmp), len(s)))
                    r_df = r_df.append(spinup(tmp))

                df = df_y.append(r_df)
                df[tbl_ships._UNIQUE_ID] = (
                        df[tbl_ships.IMO_RAW].astype(str) + df[tbl_ships._FORMATTED_DESTINATION] +
                        df[tbl_ships.STATUSLDC_RAW] + df[tbl_ships.DRAFT_RAW].astype(str)).apply(hash)
        else:
            df_n = formatter(df_n)
            df = df_n.append(df_y)
            df[tbl_ships._UNIQUE_ID] = (
                    df[tbl_ships.IMO_RAW].astype(str) + df[tbl_ships._FORMATTED_DESTINATION] +
                    df[tbl_ships.STATUSLDC_RAW] + df[tbl_ships.DRAFT_RAW].astype(str)).apply(hash)
    except Exception as e:
        output_warning(
            'Failed to performance manage parallel processing, will be single threaded instead. Exception raised: {}'.format(e))
        df_n = formatter(df_n)
        df = df_n.append(df_y)
    return df


def spinup(df):
    try:
        processes = []
        imos = df[tbl_ships.IMO_RAW].unique()
        lock = Lock()
        queue = Queue()
        mp_df = pd.DataFrame()
        iter = 1

        queue.put(mp_df)
        for imo in imos:
            tmp = df.loc[df[tbl_ships.IMO_RAW] == imo]
            process = Process(target=_formatter, args=(tmp, queue, lock, imo, iter,))
            processes.append(process)
            iter += 1

        output_info('Spawning...')
        for process in processes:
            process.start()

        output_info('Collecting...')
        for process in processes:
            process.join(timeout=10.0)

        df = queue.get()
        queue.close()
        queue.join_thread()

    except Exception as e:
        output_warning('Failed to parallelise formatter, will be single threaded instead. Exception raised: {}'.format(e))
        df = formatter(df)
    return df


def _formatter(df, queue, lock, imo, i):
    try:
            lock.acquire()
            #output_info('Processing vessel index: {} vessel IMO: {}'.format(i, imo))
            df = formatter(df)
            tmp = queue.get()
            queue.put(tmp.append(df))
            del tmp
            lock.release()
    except Exception as e:
        output_warning('Failed to format data in parallel. Exception raised: {}'.format(e))
    return df
