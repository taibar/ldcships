import settings
from settings import INPUT_ROOT as input, PROCESSED_ROOT as processed, output_info, output_warning
import pandas as pd
from definitions import tbl_ships, tbl_locode, lst_destinations
import os
from difflib import SequenceMatcher
import sqlite3

def portmanager(df):

    try:
        pf = getports()
        pf[tbl_locode._CODE] = pf[tbl_locode.COUNTRY_RAW] + ' ' + pf[tbl_locode.LOCATION_RAW]

        df.loc[df[tbl_ships._IS_LOCODE] == 'n', tbl_ships._MOST_LIKELY_PORT] = \
            df.loc[df[tbl_ships._IS_LOCODE] == 'n', tbl_ships._MOST_LIKELY_PORT].apply(lambda x: x.lower().capitalize())

        df1 = df.loc[df[tbl_ships._IS_LOCODE] == 'y'].merge(pf, how='left', left_on=tbl_ships._MOST_LIKELY_PORT,
                                                            right_on=tbl_locode._CODE)
        df2 = df.loc[df[tbl_ships._IS_LOCODE] == 'n'].merge(pf, how='left', left_on=tbl_ships._MOST_LIKELY_PORT,
                                                            right_on=tbl_locode.NAMEWODIACRITICS_RAW)

        df2 = df2.loc[(~df2[tbl_locode.STATUS_RAW].isna()) & (df2[tbl_locode.STATUS_RAW].str.startswith('A'))]
        df = df1.append(df2)
        df = df.loc[~df[tbl_locode._CODE].isna()]

        df = geolocate(df)

    except Exception as e:
        output_warning('Failed to perform port matching. Exception raised: {}'.format(e))
    return df


def getports():

    '''

    getports() queries the LOCODES database and reurns a DataFrame containing all known ports

    :return:

    '''

    try:
        output_info('Querying LOCODES database for all known ports...')
        query = "SELECT * FROM {} WHERE {} LIKE '1%'".format(settings.LOCODES_TABLE, tbl_locode.FUNCTION_RAW)
        conn = sqlite3.connect(settings.LOCODES_DB)
        df = pd.read_sql_query(query, con=conn)
        conn.close()
    except Exception as e:
        output_warning('Failed to get LOCODES from database. Exception raised: {}'.format(e))
        df = pd.DataFrame()
    return df


def geolocate(df):

    '''
    Attempts to geolocate ports using geopy
    :param df:
    :return:
    '''

    try:
        df[tbl_ships._INDICATOR] = 0

        df.loc[(~df[tbl_locode.COUNTRY_RAW].isin(['US', 'CA'])) &
               ((df[tbl_ships._DIRECTION] == 'Towards') |
                (df[tbl_ships._DIRECTION] == 'Stationary')) &
               (df[tbl_locode.COUNTRY_RAW].isin(lst_destinations.Countries)), [tbl_ships._INDICATOR]] = 1

        df.loc[(df[tbl_locode.COUNTRY_RAW].isin(['US']) & (df[tbl_locode.SUBDIVISION_RAW].isin(lst_destinations.USA))), [
            tbl_ships._INDICATOR]] = 1
        df.loc[(df[tbl_locode.COUNTRY_RAW].isin(['CA']) & (df[tbl_locode.SUBDIVISION_RAW].isin(lst_destinations.Canada))), [
            tbl_ships._INDICATOR]] = 1

    except Exception as e:
        output_warning('Unable to geolocate ports. Exception raised: {}'.format(e))
    return df

