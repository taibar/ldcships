import settings
from settings import INPUT_ROOT as input, PROCESSED_ROOT as processed, output_warning, output_info
from zipfile import ZipFile
import pandas as pd
from definitions import tbl_ships
from difflib import SequenceMatcher
import os
import numpy as np


def unpacker():

    '''

    unpacker() looks for all zip files in the input directory, and unzips them into the processed directory
    :return:

    '''

    output_info('Clearing old data files...')
    for file in os.listdir(processed):
        try:
            if file.lower().endswith('.csv'):
                os.unlink(os.path.join(processed, file))
        except Exception as e:
            output_warning('Failed to clear old processed data. Exception raised: {}'.format(e))

        output_info('Unzipping new data files...')
    for file in os.listdir(input):
        try:
            if file.lower().endswith('.zip'):
                zf = ZipFile(os.path.join(input, file), 'r')
                zf.extractall(processed)
                zf.close()
        except Exception as e:
            output_warning('Failed to unpack zipped data. Exception raised: {}'.format(e))

    return 0


def collector():

    '''

    collector() picks up the vessel file that was unpacked by unpacker()

    :return:

    '''

    df = pd.DataFrame()
    output_info('Ingesting data to dataframes...')
    try:
        tmp = pd.read_csv(settings.VESSEL_FILE, low_memory=False, usecols=tbl_ships.cols)
        df = df.append(tmp)
        del tmp
    except Exception as e:
        output_warning('Failed to collect data. Exception raised: {}'.format(e))

    df = cleaner(df)

    return df


def cleaner(df):

    '''

    cleaner(df) gets the DataFrame that was collected by collector(), and does some preliminary formatting on the data
    so that it is consistent and can be processed accurately down the line. Destinations are passed through a number
    of regex so that the destination column is composed of either LOCODES or descriptives

    :param df:
    :return:

    '''

    try:
        df = df.dropna(axis=0, how='all')
        df = df.fillna(tbl_ships.na_vals)
        df[tbl_ships._FORMATTED_DESTINATION] = df[tbl_ships.DESTINATION_RAW].str.replace(r'[^a-zA-Z0-9\s]+|X{2,}', ' ').\
            str.replace(r'\s{2,}', ' ').\
            str.replace(r'^\s{1,}', '').\
            str.replace(r'\s{1,}$', '')
        df = df.astype(tbl_ships.dtypes)
        df[tbl_ships.DATE_RAW] = pd.to_datetime((df[tbl_ships.DATE_RAW]), infer_datetime_format=True)

        df.at[df[tbl_ships.DESTINATION_RAW].str.match(settings.LOCODE_FORMAT) == True, tbl_ships._IS_LOCODE] = 'y'
        df.at[df[tbl_ships.DESTINATION_RAW].str.match(settings.LOCODE_FORMAT) == False, tbl_ships._IS_LOCODE] = 'n'

        df.at[df[tbl_ships._IS_LOCODE] == 'y', tbl_ships._FORMATTED_DESTINATION] = df.\
        loc[df[tbl_ships._IS_LOCODE] == 'y', tbl_ships._FORMATTED_DESTINATION].\
            str.replace(settings.LOCODE_CLEANER, '')


    except Exception as e:
        output_warning('Failed to clean data. Exception raised: {}'.format(e))
    return df


def formatter(df):

    '''

    formatter(df) takes the DataFrame passed by cleaner(df) and performs a fuzzy match on the descriptive destinations
    so that only the most descriptive destination is kept. This is done to remove any inconsistencies, typos, etc

    Useful for further processing, we first split the DataFrame on destinations that are known LOCODES vs those that
    are not, so that we can match only on the descriptive destinations. DataFrames are recombined after.

    If the multiproc flag is set to true, this will be called by processing_helpers.spinup(df)

    :param df:
    :return:

    '''

    try:
            fm = fuzzymatcher(df, tbl_ships._FORMATTED_DESTINATION)
            df = fromfuzzy(df, tbl_ships._FORMATTED_DESTINATION, fm)
            df = df.loc[df[tbl_ships._FORMATTED_DESTINATION] != '']
    except Exception as e:
        output_warning('Failed to format data. Exception raised: {}'.format(e))
    finally:
        return df


def fuzzymatcher(df, col):

    '''

    fuzzymatcher(df, col) is the fuzzy matching manager, allowing the fuzzy matching algorithm call on the columns of
    choice


    :param df: DataFrame
    :param col: Columne to use to fuzzymatch entries
    :return:

    '''

    entries = df[col].unique()
    fm = pd.DataFrame(index=entries)

    try:
        for entry in entries:
            ret = applymatcher(str1=entry, lst=entries)
            fm = fm.join(ret)
    except Exception as e:
        output_warning('Failed to match entries for column {}. Exception raised: {}'.format(col, e))
    return fm


def applymatcher(str1, lst):

    '''

    applymatcher(str1, lst) is the fuzzymatching algorithm trying to match a string to a list of strings.
    A score is returned for each entry in the list.

    :param str1: string to match
    :param lst: list of strings to match against
    :return:

    '''

    ret = pd.Series(name=str1, index=lst)
    for item in ret.index:
        ret.at[item] = (SequenceMatcher(None, str1, item).ratio())
    return ret


def fromfuzzy(df, col, fm):

    '''

    fromfuzzy(df, col, fm) returns the result of the matching algorithm, by keeping the highest matching entry
    :param df: source DataFrame
    :param col: Column to match
    :param fm: Fuzzy matched matrix generated by the applymatcher calls

    :return:

    '''

    try:
        _fm = pd.Series(name=tbl_ships._MOST_LIKELY_PORT, index=fm.index, dtype=np.object)
        for item in _fm.index:
            match = fm[item].loc[fm[item] == max(list(fm[item].loc[fm[item] >= settings.THRESHOLD_MATCH]))].index[0].\
                replace(' ', '')
            _fm.at[item] = match

        df = df.join(_fm, on=col)
    except Exception as e:
        output_warning('Failed to merge fuzzy matches for column {}. Exception raised: {}'.format(col, e))
    return df