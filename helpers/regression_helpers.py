import xgboost as xgb
from xgboost import XGBClassifier, plot_importance
import pandas as pd
import sklearn as sk
from sklearn.model_selection import train_test_split, KFold, cross_val_score
from sklearn.metrics import accuracy_score
from definitions import tbl_ships, tbl_locode
from settings import output_info
from matplotlib import pyplot as plt
from settings import output_warning

def builder(df):
    try:
        df = df.reset_index(drop=True)
        df = getdummies(df)

        exclude_lst = [tbl_ships.DATE_RAW, tbl_ships.DESTINATION_RAW, tbl_ships.ZONE_RAW,
                       tbl_ships._FORMATTED_DESTINATION, tbl_ships.STATUSLDC_RAW, tbl_ships.SIZE_RAW,
                       tbl_ships._MOST_LIKELY_PORT, tbl_ships._IS_LOCODE, tbl_ships._DIRECTION,
                       tbl_ships._ON_TRAJECTORY, tbl_locode._CODE, tbl_ships._INDICATOR]
        exclude_lst.extend(tbl_locode.cols)

        X = df[[i for i in list(df.columns) if not i in exclude_lst]]
        Y = df[tbl_ships._INDICATOR]

        X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=0.33, random_state=7)

        output_info('Spinning up XGBoost...')
        model = XGBClassifier()
        eval_set = [(X_train, Y_train), (X_test, Y_test)]
        eval_mt = ['logloss', 'error']
        model.fit(X_train, Y_train, eval_metric=eval_mt, eval_set=eval_set, early_stopping_rounds=40, verbose=False)
        Y_pred = model.predict(X_test)
        predictions = [round(value) for value in Y_pred]
        accuracy = accuracy_score(Y_test, predictions)
        output_info('XGBoost complete. Accuracy: %.2f%%...' % (accuracy*100.0))

        results = model.evals_result()
        epochs = len(results['validation_0']['logloss'])
        x_axis = range(0, epochs)

        fig, ax = plt.subplots()
        ax.plot(x_axis, results['validation_0']['logloss'], label='Train')
        ax.plot(x_axis, results['validation_1']['logloss'], label='Test')
        ax.legend()
        plt.ylabel('Log Loss')
        plt.title('XGBoost Log Loss')
        plt.show()
    except Exception as e:
        output_warning('Failed to run XGBoost. Exception raised: {}'.format(e))
        model = XGBClassifier()
    return model


def predict(model, df):
    try:
        df = getdummies(df)
        exclude_lst = [tbl_ships.DATE_RAW, tbl_ships.DESTINATION_RAW, tbl_ships.ZONE_RAW,
                       tbl_ships._FORMATTED_DESTINATION, tbl_ships.STATUSLDC_RAW, tbl_ships.SIZE_RAW,
                       tbl_ships._MOST_LIKELY_PORT, tbl_ships._IS_LOCODE, tbl_ships._DIRECTION,
                       tbl_ships._ON_TRAJECTORY, tbl_locode._CODE, tbl_ships._INDICATOR]

        df = df[[i for i in list(df.columns) if not i in exclude_lst]]
        df[tbl_ships._INDICATOR] = model.predict(df)
        df = df.loc[df[tbl_ships._INDICATOR]==1]
    except Exception as e:
        output_warning('Failed to predict using XGBoost. Exception raised: {}'.format(e))
        df = pd.DataFrame()
    return df


def getdummies(df):
    try:
        dummy = pd.get_dummies(df[[tbl_ships.STATUSLDC_RAW,
                                   tbl_ships._DIRECTION, tbl_ships._ON_TRAJECTORY]])
        df = df.merge(dummy, left_index=True, right_index=True)
    except Exception as e:
        output_warning('Failed to one hot encode. Exception raised: {}'.format(e))
    return df


def getsets(df, ref):
    try:
        train_set = df.loc[df[tbl_ships._UNIQUE_ID].isin(ref[tbl_ships._UNIQUE_ID].unique())]
        tmp = ref[[tbl_ships._UNIQUE_ID, tbl_ships._INDICATOR]]
        train_set = train_set.merge(tmp, how='left', left_on=tbl_ships._UNIQUE_ID, right_on=tbl_ships._UNIQUE_ID)
        test_set = df.loc[~df[tbl_ships._UNIQUE_ID].isin(ref[tbl_ships._UNIQUE_ID].unique())]
    except Exception as e:
        output_warning('Failed to split data into training and test sets. Exception raised: {}'.format(e))
        train_set = df
        test_set = pd.DataFrame()
    return train_set, test_set
