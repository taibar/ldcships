import os
import logging


#Flags===================================================================
#Clear old logs before run
CLEARLOGS           = True

#Demo mode
DEMO                = False

#Rebuild LOCODE database (because of update, loss of file, etc.)
REBUILD_DB          = False

#Run multiprocessing - should only be run if there is a performant machine
MULTIPROC           = True

#Log to stdout or file
LOG_STDOUT          = True

#Unzip and process
UNPACK              = False

#If you want to go use the fuzzy match algorithm - performance demanding
SKIPFUZZYMATCH      = False

#Run
RUN                 = True

#Plotting flag
PLOTTING            = True

#Performance flag
HIGH_POWER          = False

#Output predictions
PREDICT             = True

#Threshold settings======================================================
#Fuzzy match threshold
THRESHOLD_MATCH     = 0.8

#Number of days to consider
NBR_DAYS            = 30

#Max allowable draft (based off of a shipping news website)
MAX_DRAFT           = 14.33

#Earth radius in km
RADIUS              = 6371

#Assumed radius of waiting area around Panama entrance
WAITING_AREA        = 50

#Offcourse threshold
OFFCOURSE_THRESH    = .10

#If HIGH_POWER is set to false, set the number of lines to process at a time
NBR_LINE            = 20000

#Important Coordinates===================================================
PANAMA_LAT          = 9.1173
PANAMA_LONG         = -79.7188

#Regex terms=============================================================
LOCODE_FORMAT       = r'\b([a-zA-Z]{2}[\s][a-zA-Z][a-zA-Z0-9]{2})\b'
LOCODE_CLEANER      = r'(?<=[a-zA-Z]{2}\s[a-zA-Z]{1}[a-zA-Z0-9]{2}).*$'

#Paths & Directories=====================================================
PROJECT_ROOT        = os.path.abspath(os.path.dirname(__file__))
DATA_ROOT           = os.path.abspath(os.path.join(PROJECT_ROOT, "data"))
INPUT_ROOT          = os.path.abspath(os.path.join(DATA_ROOT, "input"))
PROCESSED_ROOT      = os.path.abspath(os.path.join(DATA_ROOT, "processed"))
REFERENCE_ROOT      = os.path.abspath(os.path.join(DATA_ROOT, "reference"))
OUTPUT_ROOT         = os.path.abspath(os.path.join(DATA_ROOT, "output"))
LOGS_ROOT           = os.path.abspath(os.path.join(PROJECT_ROOT, "log"))
DEFINITIONS_ROOT    = os.path.abspath(os.path.join(PROJECT_ROOT, "definitions"))

LOCODES_DB          = os.path.abspath(os.path.join(REFERENCE_ROOT, "locodes.sqlite"))
LOCODES_TABLE       = 'LOCODES'
VESSEL_FILE         = os.path.abspath(os.path.join(PROCESSED_ROOT, "exoPanama.csv"))

#Logging=================================================================
LOGGER = logging.getLogger()

#Logger format
LOGGER.setLevel(logging.INFO)
handler = logging.FileHandler(os.path.join(LOGS_ROOT, 'info.log'))
handler.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
LOGGER.addHandler(handler)


if LOG_STDOUT:
    output_info = print
    output_warning = print
else:
    output_info = LOGGER.info
    output_warning = LOGGER.warning

#Word list to designate transit through Panama Canal=====================
TRANSIT_LIST        = ['PANAMA', 'P', 'PNM', 'PA', 'PAN','PANAM']