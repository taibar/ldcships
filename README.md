# LDCSHIPS

ldcships is a classifier that determines if a vessel will be going through the Panama Canal within 30 days of its last known position. The classifier first does substantial data processing and then classifies vessels using XGBoost.

##Dependencies
This project was developed in Python 3.7 with the following external dependencies:

* Numpy
* Pandas
* sqlite3
* Scikit-Learn
* XGBoost
* Virtualenv

The classifier is able to run standalone but should be set up to run in a virtualenv. In doing so, the dependencies can be managed using pip and the requirements.txt file which contains a list of all dependencies for this project.

##Point of Entry
Unprocessed zips can be placed in data/input, and the UNPACK flag can be set to True for the first run (set to False for subsequent runs to save time).

The zip file provided for this project is included herein. However, functionality is built in for any other zip.

Point of entry is main() in main.py

##Point of Exit
Predicted vessels are stored as csv under predicted_ships.csv, and rejected ships are stored as rejected_ships.csv. Both files can be found in data/output.

##Installing and Running the Package
The package contains all necessary data to run, and the settings to the package have been set to process the data.

###Setup
####Build and run

This is the recommended approach as all the back end work is taken care of. In a terminal, input:

git clone https://bitbucket.org/taibar/ldcships.git && cd ldcships

then if this is the first time you have used this package, input:

bash run.sh -b

the -b flag will build the package and install all necessary dependencies before running the package.

If you have set up all the packages and would like to run the package, input:

bash run.sh -r

If you want to know more about run.sh, call it with the -h flag, input:

bash run.sh -h


####Manual set up
If you are installing the package for the first time, or want full control on the installed packages, the easiest way to get the package is to follow the steps below.

If you do not have virtualenv installed on your machine, go ahead and do so:

pip3 install virtualenv

Once virtualenv is installed, clone the repo and cd to the directory:

git clone https://bitbucket.org/taibar/ldcships.git && cd ldcships

From here, let's set up our virtualenv:

virtualenv venv

Now activate the virtualenv:

source venv/bin/activate

Now that you are in your virtualenv, let's install all the dependencies:

pip3 install -r requirements.txt

###Run
If your environment is all setup, from the ldcships directory simply call

python3 -m main.py

##Background
Vessels in the Pacific ocean and heading east can either load in the west coast of North America, or go to US Gulf through the Panama Canal.

The following data is provided:

* __imo__: the unique identifier by vessel
* __date__: the date at which the observation was made
* __longitude, latitude__: the daily position of the vessel
* __bearing__: the direction in which the vessel is going (computed from 2 consecutive points)
* __destination__: the destination input manually by the master of the vessel
* __zone__: True if the vessel is in polygon (CP), False if it is in (P)
* __draft__: the draft of the fleet
* __statusLDC__: the status of the fleet (laden,ballast,unsure)
* __size__: the size of the vessel

![quadrants](extras/map_of_quadrants.png)

##Date Preprocessing
In order to build a model that can correctly predict if a vessel will pass through the canal, the first step is to preprocess the data and label the journeys. This achieves both a refinement in scope and better defines the journey's features. The below labelling exercise is all undertaken by the program with no requirements to label the data manually.

###Filtering the initial data
The dataset is first cleaned using a regex to reformat destination entries so only text or text with a digit suffix can be used in processing. The data is then filtered to remove any vessel that will clearly not go through the canal:

* The vessel cannot be larger than a Panamax
* The vessel cannot have a draft greater than 14.33m, laden or ballast, given that that is the maximum allowable draft in the canal

Any vessel in the CP polygon is also removed as it is not deemed going through the Canal from West to East.

###Cleaning the filtered data
The destination entries that are provided are entered manually by the master of his respective vessel. This means that there are numerous inconsistencies with the destinations recorded by the master i.e. Panama Balboa could be referred to as PA BLB, PABLB, Balboa, etc.

Because of the volume of data that still needs to be processed at this point, a multicompute fuzzy matching algorithm processes each vessels' most likely destination. This garantees consistent destination entries, and improves the likelihood that a destination input by the master will be something can be use to match against UN LOCODES.

###Augmenting the data
The data is augmented with UN LOCODES so that the geographical destination of the vessel can be determined. Once the UN LOCODES are joined to the data, a further round of filtering is applied if the join introduces a many to one relationship: a vessel that will go through the Canal will have to pay the freight prices of going through. Therefore, to benefit from economies of scale, only large vessels are assumed to be going through for commercial purposes. If the vessel is large, the likelihood that it will need substantial infrastructure at destination is high, therefore any destination that is not reccognised internationally, as per the UN LOCODE spec, is discarded. This filtering should only be applied to vessels whose destination LOCODE results in more than one unique case, following the join.

###Feature extraction
Furthermore, the data can also show if a vessel is stationary or moving by calculating the Haversine distance of the vessel to the canal at each timestamp; if the distance changes, the vessel is moving and if the distance does not change it is clearly stationary.

In the case that the vessel is moving, based on the trajectory of the vessel, it is possible to determine if the vessel is heading towards the canal or not. This supplements the bearing field provided.


###Labelling the data
According to the Panama Canal website, the vast majority of vessels cutting through the canal West to East head towards the North American East coast, Europe, and to a certain extent, the South American Atlantic coast. To this end, a list of these regions is created by country/state/province, and this list is used to label all remaining vessels. If a vessel remains in this dataset and has a destination within the described regions, it is labelled as going through the Canal, otherwise it is labelled as not going the 
Canal.

##Building the Predictive Model

###Overview of XGBoost
XGBoost is a gradient boosted decision tree model that has been used with great success in the field of machine learning. The model is fast and performs well with classification problems.

###Model performance and results
The model was run on the full dataset and returned an accuracy of just over 96%. The logloss was also reduced quite well:

![results](extras/lossplot.png)

Classification on qualifying entries (note that this represents any record following the initial filtering - the full set contains 565,602 records, although only 267,149 records qualify after initial filtering) leads to a split of 251,704 records for vessels predicted not going through the Canal, and 15,445 records for vessels going through the Canal.

###Model shortcomings
There are a few shortcomings to the model that could be addressed in future works. Firstly, the entire process takes a very long time to run (about 1.5 hours), although most of the time is spent preprocessing the data. This makes it difficult to make quick assessments. In the future, perhaps using GPU acceleration could be useful, or a whole rearchitecture would be necessary.

Secondly, the model does appear to overfit. Some model fine tuning was conducted, including early termination and reduction of the number of features to avoid overfitting. However this tuning was done on runs with smaller sets, given how long it takes to process the data on a full run. More work could be done in this regard to improve the model in the future.

Thirdly, no cross validation was conducted on the model. However, a cursory look through the output files would indicate that the model predicts well enough for the purpose of this exercise.
 